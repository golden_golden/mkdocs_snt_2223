---
title : "Introduction"
correctionvisible : false

---

# Folium 

Nous allons utiliser les cartes proposées par Open Street Map et le langage Python afin de générer des cartes personnalisées.

Plus exactement, nous allons utiliser une bibliothèque Python nommée Folium. Une bibliothèque Python permet de rajouter des fonctionnalités au langage de base. 

Folium va donc nous permettre de créer nos propres cartes à partir des cartes proposées par Open Street Map.


Créez un dossier et nommez-le par exemple "carto_OSM" 


## Créer une carte

Le fichier devra être enregistré dans le répertoire créé au "À faire vous-même 1" (le fichier pourra être nommé "carte_1.py"). 

``` py
import folium
macarte = folium.Map(location=[50.7517,5.9106], zoom_start=13)
macarte.create_map(path='macarte.html')
```


Il est possible de remarquer ici:

- que le fond de carte provient d'OpenStreetMap (par défaut), qu'il est centré sur les coordonnées en WGS84, 50.7517,5.9106 et que j'ai choisi un zoom de départ de 13;
- d'autres fonds sont possibles (tuiles OpenStreetMap, Stamen Terrain, Stamen Toner, Cloudemade et Mapbox avec une clé, par défaut), ou n'importe quel fond supporté par Leaflet:


## Créer un marker
L'intérêt est évidemment l'ajout de données vectorielles sur ce fond. Cela peut se faire de deux manières:

- avec des simples marqueurs (avec popup ici). Plusieurs sont disponibles, voir Folium: Circle Markers par exemple:


``` py
monpoint = folium.Map(location=[50.7517, 5.9106],zoom_start=13)
monpoint.simple_marker(location=[50.7517, 5.9106], popup='3290005')
monpoint.create_map(path='point.html')
```


## Créer une forme géométrique

## Utiliser des fichiers aux formats GeoJSON ou TopoJSON
- à l'aide de fichiers aux formats GeoJSON ou TopoJSON, voir GeoJSON, nouveau lingua franca en géomatique ?

``` py
#fichier au format GeoJSON
mongeojson = "aachen.geojson"
carte = folium.Map(location=[50.63423,5.8303],tiles='Mapbox Bright', zoom_start=12)
carte.geo_json(geo_path=mongeojson)
carte.create_map(path='aachen.html')
```

- les polygones:












[https://pixees.fr/informatiquelycee/n_site/snt_carto_osmPerso.html](https://pixees.fr/informatiquelycee/n_site/snt_carto_osmPerso.html)
[https://pixees.fr/informatiquelycee/n_site/snt_carto_osm.html](https://pixees.fr/informatiquelycee/n_site/snt_carto_osm.html)


!!! note "ressources"
    - [https://portailsig.org/content/python-leaflet-folium-ou-comment-creer-des-cartes-interactives-simplement.html](https://portailsig.org/content/python-leaflet-folium-ou-comment-creer-des-cartes-interactives-simplement.html)

    - [https://www.youtube.com/watch?v=beoKG-ZYwcg](https://www.youtube.com/watch?v=beoKG-ZYwcg)


    - [https://pod.phm.education.gouv.fr/video/23378-folium-et-python-exo1mp4/](https://pod.phm.education.gouv.fr/video/23378-folium-et-python-exo1mp4/)




!!! faq correction "Correction"
    bonjour




<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  