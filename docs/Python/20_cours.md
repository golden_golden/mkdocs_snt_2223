---
title : "Le language Python"

---

# Introduction au language Python

## 1. Les types
### 1.1 Les différents types
En informatique, on traite de façon automatique des données. Cependant toutes les données ne sont pas toutes de la même nature. Vous conviendrez que l’heure de votre réveil et votre dernier SMS reçu n’ont pas la même nature : le premier correspond à des nombres et le second à du texte.

<br/>

Le soucis est que le traitement de l'information peut être différent suivant la nature de l'information, en informatique on dit que les données ont des types différents.

<br/>

Les types peuvent être classé en deux catégories :

- Les types simples : `int, float, str, bool, …`
- Les types construits : `list, …`

Cette années nous étudierons quelques types de base du language Python, cependant il en existe bien d'autres !



### 1.2 Les types numériques : `int` et `float`
#### A. Définitions
!!! note "Le type entier" 
    Le *type entier* désigne les entiers relatifs (positifs ou négatifs).
    En Python on parle du type `int` (pour *integer* qui signifie « entier » en anglais) ;

!!! note "Le type flottant"
    Le *type flottant* désigne les nombres décimaux (à virgule).
    En Python, on parle du type `float` (pour l'abréviation de "floating number" qui signifie "nombre à virgule flottante" en anglais).

#### B. Les opérateurs mathématiques
Nous nous contenterons ici de montrer quelques opérations possibles sur les entiers et les flottants.

<br/>

Pas de surprise : il est possible d’additionner, soustraire, multiplier, diviser des variables de type nombre (entier ou flottant). On utilise pour cela les symboles habituels résumés dans le tableau ci-dessous dans lequel on indique aussi comment élever un nombre à une certaine puissance.

<center>

Opération | Écriture en Python 
--------- |------------------- 
Addition  |	`a + b`
Soustraction |	`a - b`
Multiplication |	`a * b`
Division |	`a / b`
Division entière |	`a // b`
Élever `a` à la puissance `n` |	`a ** n` (double symbole de multiplication)


</center>




### 1.3 Type textuel : `str`
#### A. Définitions
!!! note "Le type chaîne de caractères"
    Le type chaîne de caractères désigne toute suite ordonnée de caractères.

    En Python on parle du type `str` (pour *string* qui signifie « chaîne » en anglais).


#### B. Les opérateurs avec `str`
Il est également possible d'utiliser les opérateurs mathématiques sur les chaînes de caractères mais le traitement sera évidement différent.


- la concaténation qui consiste à mettre bout à bout deux chaînes pour former une unique chaîne : on utilise pour cela le symbole d’addition `+`.

???+ example "Exemple"
    ```python
    >>>"bonjour" + "le monde"
    bonjour le monde 
    ```

<br/>

- la répétition qui consiste à ajouter une chaîne un certain nombre de fois à elle-même : on utilise pour cela le symbole de multiplication `*`.

???+ example "Exemple"
    ```python
    >>>"bonjour " *3
    bonjour bonjour bonjour 
    ```

#### C. Les f-string


Les f-strings permettent d'introduire la valeur d'une variables dans une chaîne de caractère à l'aide d'une syntaxe lisible et intuitive.

Pour écrire une f-string, il faut tout simplement précédée la chaîne de caractères du caractère `f` sans espace entre les deux :
Ce caractère `f` avant les guillemets va indiquer à Python qu'il s'agit d'une f-string permettant de mettre en place le mécanisme de l'écriture formatée, contrairement à une string normale.

<br/>

Pour inclure une variable dans la f-string, il suffit d'écrire le nom de la variable entre accolade `{}`.

???+ example "Exemple"
    ```python
        >>> x = 32
        >>> nom = "John"
        >>> print(f"{nom} a {x} ans")
        John a 32 ans
    ```

    <br/>

    La f-string `f"{nom} a {x} ans"` à donné `John a 32 ans`. 
    
    On notera que bien que le contenue de la variable `x`, soit de type `int`, cela n'as pas posé de problème à la fonction `print()` et aucune manipulation n'as été nécessaire *(pas besoin d'utiliser une fonction `str()`)*.


<br/>

Une fonctionnalité extrêmement puissante des f-strings est de supporter des expressions Python au sein des accolades. Ainsi, il est possible d'y mettre directement une opération ou encore un appel à une fonction :

???+ example "Exemple"
    ```python
        >>> entier = 2
        >>> print(f"Le type de {entier} est {type(entier)}")
        Le type de 2 est <class 'int'>
    ```

<br/>

**Écriture scientifique formaté**

Pour les nombres très grands ou très petits, l'écriture formatée permet d'afficher un nombre en notation scientifique *(sous forme de puissance de 10)* avec la lettre `e` :

???+ example "Exemple"
    ```python
        >>> print(f"{1_000_000_000:e}")
        1.000000e+09

        >>> print(f"{0.000_000_001:e}")
        1.000000e-09
    ```

<br/>

Il est également possible de définir le nombre de chiffres après la virgule. Dans l'exemple ci-dessous, on affiche un nombre avec aucun, 3 et 6 chiffres après la virgule :

???+ example "Exemple"
    ```python
        >>> avogadro_number = 6.022_140_76e23
        >>> print(f"{avogadro_number:.0e}")
        6e+23

        >>> print(f"{avogadro_number:.3e}")
        6.022e+23

        >>> print(f"{avogadro_number:.6e}")
        6.022141e+23
    ```

Les possibilités des f-string sont très nombreuses, pour vous y retrouver dans les différentes options de formatage, nous vous conseillons de consulter [ce mémo](https://fstring.help/cheat/) *(en anglais)*.

### 1.4 Le type booléens : `bool`
#### A. Définitions
Les booléens sont un type particulier de variables qui n’ont que deux valeurs possibles :
```python
True: Vrai
False: Faux
```

#### B. Les opérateurs booléens

<center>

Opérations| Symboles
--------- |----------
    ET    |    `and`
    OU    |    `or`
    NON   |    `not`

</center>


### 1.5 Type construit tableau : `list`
#### A. Définitions
Une liste est une structure de données qui contient une série de valeurs. Python autorise la construction de liste contenant des valeurs de types différents (par exemple entier et chaîne de caractères), ce qui leur confère une grande flexibilité. Une liste est déclarée par une série de valeurs (n'oubliez pas les guillemets, simples ou doubles, s'il s'agit de chaînes de caractères) séparées par des virgules et le tout encadré par des crochets.

???+ example "Exemple"
    ```python
    >>> animaux = ["girafe", "tigre", "singe", "souris"]
    >>> tailles = [5, 2.5, 1.75, 0.15]
    >>> mixte = ["girafe", 5, "souris", 0.15]
    >>> animaux
    ['girafe', 'tigre', 'singe', 'souris']
    >>> tailles
    [5, 2.5, 1.75, 0.15]
    >>> mixte
    ['girafe', 5, 'souris', 0.15]

    ```

#### B. Utiliser les listes
Un des gros avantages d'une liste est que vous pouvez appeler ses éléments par leur position. Ce numéro est appelé indice (ou index) de la liste.

```python
liste  : ["girafe", "tigre", "singe", "souris"]
indice :        0        1        2         3
```

<br/>

Soyez très attentif au fait que les indices d'une liste de n éléments commencent à `0` et se terminent à `n-1`. 

???+ example "Exemple"

    ```python
    >>> animaux = ["girafe", "tigre", "singe", "souris"]
    >>> animaux[0]
    'girafe'
    >>> animaux[1]
    'tigre'
    >>> animaux[3]
    'souris'
    ```

    Par conséquent, si on appelle l'élément d'indice 4 de notre liste, Python renverra un message d'erreur :

    ```python
    >>> animaux[4]
    Traceback (innermost last):
      File "<stdin>", line 1, in ?
    IndexError: list index out of range
    ```

N'oubliez pas ceci ou vous risquez d'obtenir des bugs inattendus !

<br/>

??? note "Liste de listes"
    Les éléments d'une liste peuvent êtres des variables et même d'autres listes ! On peut alors utiliser les indices.

    
    ```python
    >>> couleur = ["rouge", "vert", "bleu", "violet"]
    >>> animaux = ["girafe", "tigre", "singe", "souris"]
    >>> fruit = ["pomme", "orange", "poire", "prune", "cerise", "ananas"]
    >>> liste_de_liste = [couleur, animaux, fruit]
    >>> liste_de_liste[1]
    ["girafe", "tigre", "singe", "souris"]

    >>> liste_de_liste[1][3]
    souris
    ```



#### C. Manipulation des listes avec : les opérateurs
Tout comme les chaînes de caractères, les listes supportent l'opérateur `+` de concaténation, ainsi que l'opérateur `*` pour la duplication :

???+ example "Exemple"
    ```python
    >>> ani1 = ["girafe", "tigre"]
    >>> ani2 = ["singe", "souris"]
    >>> ani1 + ani2
    ['girafe', 'tigre', 'singe', 'souris']
    >>> ani1 * 3
    ['girafe', 'tigre', 'girafe', 'tigre', 'girafe', 'tigre']
    ```

L'opérateur `+` est très pratique pour concaténer deux listes.





#### D. Manipulation des listes avec : les méthodes

*cf. partie  `3. Fonctions et méthodes (partie 1)` dans la suite de ce cours.*



## 2. Variables et affectation
### 2.1 Qu'est ce qu'une variable
!!! def "Variable"
    Une variable est une référence à un espace de stockage de la mémoire (une case mémoire) dans lequel il est possible de stocker une valeur (une donnée).

    Chaque variable est caractérisée par **son nom**, **son type** (en python le type de la donnée quelle contient) et **sa valeur**.

### 2.2 Noms de variable
Chaque variable possède un nom qui permet d’identifier l’emplacement mémoire correspondant.

<br/>

Dans le langage Python, il y a des règles à respecter pour nommer les variables. Voici celles qui vous concernent :

!!! important "Nom de variable"
    - **Règle 1 :** un nom ne peut contenir que des lettres (a-z, A-Z), des chiffres (0 - 9) et le caractère _ (underscore).
  
    - **Règle 2 :** un nom ne peut pas commencer par un chiffre.
  
    - **Règle 3 :** les noms sont sensibles à la casse, cela signifie qu’il y a une distinction entre les minuscules et les majuscules : la variable nommée `snt` est différente de la variable `SNT` , et de la variable `Snt`.
    
    - **(Règle 4) :** il est préférable de toujours choisir un nom de variable représentatif : par exemple, si vous voulez stocker le nom d’une personne dans une variable, il est préférable de l’appeler `nom` plutôt que `x`.
    
    - **(Règle 5) :** il est préférable de ne pas utiliser de caractères accentués dans le nom d’une variable (nous n’entrerons pas dans le pourquoi du comment).

??? note "Remarque"
    En Python, le symbole underscore (c’est-à-dire _) est très souvent utilisé pour marquer une séparation entre plusieurs mots dans un nom : si on veut utiliser une variable qui contiendra un nombre d’élèves de Seconde G, on peut la nommer `nombre_eleves_2G` ou encore `nb_eleves_2G` car c’est plus facile à lire que `nombreeleves2G`.

??? faq "Exercice"

    Quels sont les noms de variables incorrects parmi ceux proposés ? Vous indiquerez pourquoi.
  
    - `prix achat`
    - `prix_achat`
    - `note`
    - `2ndeG`
    - `SecondeG`
    - `Seconde:G`
    - `dix-huit`
  
    Proposez un nom de variable permettant de stocker :

    - le nombre de filles de Seconde 9
    - le tarif d’un repas au self
    - l’aire d’un triangle (il n’y a qu’une seule figure)
    - la note à un devoir d’anglais

### 2.3 Affectation
Chaque variable possède une valeur qui est l’information qu’elle porte. Par exemple, la valeur de la variable `nombre_eleves_2G` pourrait être `34`, celle de la variable `note` pourrait être `14.5`, celle de la variable `prenom` pourrait être `"Louane"`.

<br/>

En Python, pour définir ou modifier la valeur d’une variable c’est très simple, il suffit d’utiliser le symbole égal : `=`.

???+ example "Exemple"
    Je souhaite que ma variable `note` soit égale à `15`, j’écris :

    ```python
    note = 15
    ```

    **Remarque :** Dans ce cas, la variable est de type entier (`int` en Python).

    <br/><br/>

    Si finalement, mon professeur a oublié un demi-point, je peux modifier sa valeur comme ceci :
    ```python
    note = 15,5
    ```

    **Remarque :**

    - On utilise le point (.) et non pas la virgule pour écrire des nombres décimaux.
    - Dans ce cas, le type de la variable note a changé : c’est devenu un flottant (`float` en Python).



## 3. Fonctions et méthodes (partie 1)

### 3.1 Reconnaître et utiliser une fonction
Pour utiliser une fonction, il suffit d'utiliser la syntaxe des fonctions, c'est à dire le nom de la fonction suivi de parenthèse : `nom()`.

<br/>

Il peu éventuellement y avoir des paramètres (ou arguments) à l’intérieur des parenthèses. On note alors : `nom(paramètre1, paramètre2,…)`


Un paramètre est une donnée nécessaires au bon fonctionnement de la fonction. Il peut y avoir autant de paramètre que nécessaires ! Les paramètres peuvent également êtres des variables.


???+ example "Exemple"
    ```python 
    # Soit la fonction `addition(entier1, entier2)` qui renvoie le résultat de l'addition de deux entiers"
    >>> addition(5,2)
    7

    >>> a = 15
    >>> b = 20
    >>> addition(a,b)
    35
    ```

!!! note "Remarque"
    Il existe de très, très, très nombreuses fonctions et méthodes en Python, il est impossible de les connaître toutes ! Il suffit d'être capables de chercher/trouver les fonctions suivant ses besoins et de savoir les mettre en œuvre.

    <br/>

    Dans la suite de ce cours, nous allons voir comment mettre en œuvre, les méthodes et les fonctions en étudiant quelques exemples, parmi les plus souvent rencontré.
    
    <br/>

    Pour trouver les fonctions et les méthodes, il est possible de se référer aux documentations de python et des modules.
    



### 3.2 Des fonctions pour intéragir avec l'utilisateur
#### A. Fonction `print()`
Pour afficher la valeur d’une variable, on utilise la fonction `print()`. Par exemple, pour afficher la valeur de la variable `note` on écrit simplement : `print(note)`. À l’exécution du code, la valeur s’affiche dans la console.

???+ example "Exemple"
    ![console](images/print.png)

----
modif taille images

----


#### B. Fonction `input()`
La fonction `input()` permet au programme de récupérer un texte saisie par l’utilisateur. Il est alors possible de mémoriser cette donnée dans une variable.

???+ example "Exemple"
    ```python
    nom = input()

    ```

<br/>

On notera qu'il est possible d'afficher un message à l'utilisateur, en passant un texte en paramètre.

???+ example "Exemple"
    ```python
    # Récupère une chaîne de caractère
    nom = input("Comment vous appelez-vous?")

    ```


    Lorsque le programme est lancé, l’utilisateur devra répondre aux questions posées. On pourra alors utiliser ces réponses grâce aux variables.

<br/>

On oubliera pas de changer le type, en utilisant des fonctions de conversion pour correspondre aux besoins du programme (`cf. partie 3.4 Des fonctions sur les types`).

???+ example "Exemple"
    ```python
    annee_courante =  2022

    # On converti la réponse de l'utilisateur en entier avec la fonction int
    age = int(input("Quel âge avez-vous ?"))

    # On calcule la valeur de l'année de naissance de l'utilisateur
    date_naissance = annee_courante - age

    # On affiche une réponse dans la console
    print(f"Votre année de naissances est {date_naissance}.")

    ```

    Pour utiliser la donnée présente dans la variable `age`, il est nécessaire de changer son type en `int`, car on ne peut pas effectuer de calcul avec un `str`. L'utilisation d'une f-string pour afficher le résultat, nous évite de devoir également changer le type de la variable `date_naissance`, en `str` avec la fonction `str()`.



### 3.4 Des fonctions sur les types
#### A. La fonction `type()`
Si vous ne vous souvenez plus du type d'une variable, utilisez la fonction `type()` qui vous le rappellera.

???+ example "Exemple"
    ```python
    >>> x = 2
    >>> type(x)
    <class 'int'>

    >>> y = 2.0
    >>> type(y)
    <class 'float'>

    >>> z = '2'
    >>> type(z)
    <class 'str'>

    ```

#### B. Fonction pour changer de type
En programmation, on est souvent amené à convertir les types, c'est-à-dire passer d'un type numérique à une chaîne de caractères ou vice-versa. En Python, rien de plus simple avec les fonctions `int()`, `float()` et `str()`.


???+ example "Exemple"
    ```python
    >>> i = 3
    >>> str(i)
    '3'

    >>> i = '456'
    >>> int(i)
    456

    >>> float(i)
    456.0

    >>> i = '3.1416'
    >>> float(i)
    3.1416
    ```
<br/>

Il existe également *(Le constructeur)* la fonction `list()` qui renvoie une liste.

- Si aucun paramètre n’est transmis, il renvoie une liste vide.
- Si un itérable ( comme une chaîne de caractère) est passé en paramètre, il crée une liste composée des éléments de l’itérable.


???+ example "Exemple"
    ```python
    >>> une_liste_vide = list()
    >>> une_liste_vide
    []

    >>> un_texte = "abcdefghijclmnopqrstuvwxyz"
    >>> liste_alphabet = list(un_texte)
    >>> liste_alphabet
    ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'c', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

    ```




### 3.5 Des fonctions communes aux listes et aux chaines de caractères
#### A. Fonction `len()`

L'instruction `len()` vous permet de connaître la longueur d'une liste, c'est-à-dire le nombre d'éléments que contient la liste. Voici un exemple d'utilisation :


???+ example "Exemple"
    ```python
    >>> animaux = ["girafe", "tigre", "singe", "souris"]
    >>> len(animaux)
    4

    >>> len([1, 2, 3, 4, 5, 6, 7, 8])
    8
    ```


### 3.6 Des fonctions sur les listes
#### A. Fonctions `max()` et `min()`
La fonction `max()` renvoie la plus grande valeur d’une série de données.

La fonction `min()` renvoie la plus petite valeur d’une série de données.

???+ example "Exemple"
    ```python
    >>> notes = [10, 12, 8, 14, 10, 9]
    >>> max(notes)
    14

    >>> min(notes)
    8
    ```

#### B. Fonctions `sum()`
La fonction `sum()` ne fonctionne qu’avec des valeurs numériques. 

Si vous essayez de l’utiliser avec un type non numérique, vous obtiendrez une erreur.


???+ example "Exemple"
    ```python
    >>> sum([10.2, 4.3, 8.0, 14, 10])
    46.5


    >>> notes = [10, 12, 8, 14, 10, 9]
    >>> sum(notes)
    63
    ```

    On notera que :

    - si une des données est de type `float`, le résultat renvoyer par la fonction `sum()` sera également de type `float`.
    - si les données sont toutes de type `int` alors le résultat sera également de type `int`.



#### C. Fonctions `sorted()`
La fonction `sorted()` trie également une liste. Cette fonction renvoie la liste triée et ne modifie pas la liste initiale *(Contrairement à la méthode`.sort()`)* :

???+ example "Exemple"
    ```python
    >>> liste_initiale = [3, 1, 2]
    >>> sorted(liste_initiale)
    [1, 2, 3]

    >>> liste_initiale
    [3, 1, 2]
    ```

    La liste `liste_initiale` n'as pas été modifiée !

<br/>

La fonction `sorted()` supporte l'argument `reverse=True` pour inverser l'ordre :

???+ example "Exemple"
    ```python
    >>> liste_initiale = [3, 1, 2]
    >>> sorted(liste_initiale)
    [1, 2, 3]
    >>> sorted(liste_initiale, reverse=True)
    [3, 2, 1]

    ```

#### D. Les fonctions `range()` et `list()`

L'instruction `range()` est une fonction spéciale en Python qui génère des nombres entiers compris dans un intervalle. Lorsqu'elle est utilisée en combinaison avec la fonction `list()`, on obtient alors une liste d'entiers.

???+ example "Exemple"
    ```python
    >>> list(range(10))
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    ```

    La commande `list(range(10))` a généré une liste contenant tous les nombres entiers de 0 inclus à 10 exclu.

<br/>

*Nous verrons l'utilisation de la fonction `range()` toute seule dans la partie sur les boucles.*

<br/>


Dans l'exemple ci-dessus, la fonction `range()` a pris un argument, mais elle peut également prendre deux ou trois arguments.

???+ example "Exemple"
    ```python
    >>> list(range(0, 5))
    [0, 1, 2, 3, 4]

    >>> list(range(15, 20))
    [15, 16, 17, 18, 19]

    >>> list(range(0, 1000, 200))
    [0, 200, 400, 600, 800]

    >>> list(range(2, -2, -1))
    [2, 1, 0, -1]
    ```

<br/>


L'instruction `range()` fonctionne sur le modèle `range([début,] fin [, pas])`. Les arguments entre crochets sont optionnels. Pour obtenir une liste de nombres entiers, il faut l'utiliser systématiquement avec la fonction `list()`.

Enfin, prenez garde aux arguments optionnels, par défaut (0 pour début et 1 pour pas) :

???+ example "Exemple"
    ```python
    >>> list(range(10,0))
    []
    ```

    <br/>

    Ici la liste est vide car Python a pris la valeur du pas par défaut qui est de 1. Ainsi, si on commence à 10 et qu'on avance par pas de 1, on ne pourra jamais atteindre 0. Python génère ainsi une liste vide.
    
    Pour éviter ça, il faudrait par exemple, préciser un pas de -1 pour obtenir une liste d'entiers décroissants :

    ```python
    >>> list(range(10,0,-1))
    [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
    ```


??? note "vidéo"
    <center><iframe width="560" height="315" src="https://www.youtube.com/embed/6XNuUZ7Z5tc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><center/>



### 3.7 D'autres fonctions
#### A. La fonction eval()
La fonction `eval()` nous permet d’exécuter des chaînes de caractères en tant que instruction Python. Elle accepte une chaîne de caractère et retourne un objet.

???+ example "Exemple"
    ```python
    >>> eval("2 > 1")
    True


    >>> eval("8 + 2")
    10


    >>> eval("'WayTo' + 'LearnX'")
    'WayToLearnX'
    >>> eval("max(1, 3, 2)")
    3


    >>> eval('"welcome".upper()')
    'WELCOME'
    ```


### 3.8 Des méthodes pour les listes

Les listes possèdent de nombreuses méthodes qui leur sont propres et qui peuvent se révéler très pratiques. 

Une méthode est une sorte de fonction qui agit sur l'objet auquel elle est attachée par un point..

<br/>

La syntaxe est : `nom_de_la_liste.nom_de_la_methode()`



#### A. .append()

La méthode `.append()`, ajoute un élément à la fin d'une liste.


???+ example "Exemple"
    ```python
    >>> liste_initiale = [1, 2, 3]
    >>> liste_initiale.append(5)
    >>> liste_initiale
    [1, 2, 3, 5]
    ```


#### B. Insérer un élément .insert()

La méthode `.insert()` insère un objet dans une liste à un indice déterminé, par conséquent elle prend deux paramètres `.insert(indice, objet)`

???+ example "Exemple"
    ```python
    >>> liste_initiale = [1, 2, 3]
    >>> liste_initiale.insert(2, -15)
    >>> liste_initiale
    [1, 2, -15, 3]
    ```





#### C. .remove()

La méthode `.remove()` supprime un élément d'une liste à partir de sa valeur.

???+ example "Exemple"
    ```python
    >>> liste_initiale = [1, 2, 3]
    >>> liste_initiale.remove(3)
    >>> liste_initiale
    [1, 2]
    ```

<br/>

S'il y a plusieurs fois la même valeur dans la liste, seule la première est retirée. Il faut appeler la méthode `.remove()` autant de fois que nécessaire pour retirer toutes les occurrences d'un même élément.

???+ example "Exemple"
    ```python
    >>> liste_initiale = [1, 2, 3, 4, 3]
    >>> liste_initiale.remove(3)
    >>> liste_initiale
    [1, 2, 4, 3]

    >>> liste_initiale.remove(3)
    >>> liste_initiale
    [1, 2, 4]
    ```


#### D. .sort()

La méthode `.sort()` trie les éléments d'une liste du plus petit au plus grand.

???+ example "Exemple"
    ```python
    >>> liste_initiale = [3, 1, 2]
    >>> liste_initiale.sort()
    >>> liste_initiale
    [1, 2, 3]
    ```

<br/>

L'argument `reverse=True` spécifie le tri inverse, c'est-à-dire du plus grand au plus petit élément.

???+ example "Exemple"
    ```python
    >>> liste_initiale = [3, 1, 2]
    >>> liste_initiale.sort(reverse=True)
    >>> liste_initiale
    [3, 2, 1]
    ```



#### E. .reverse()

La méthode `.reverse()` inverse une liste.

???+ example "Exemple"
    ```python
    >>> liste_initiale = [3, 1, 2]
    >>> liste_initiale.reverse()
    >>> liste_initiale
    [2, 1, 3]
    ```



#### F. .count()

La méthode `.count()` compte le nombre d’occurrence dans une liste, aillant la valeur passés en argument.

???+ example "Exemple"

    ```python
    >>> liste_initiale = [1, 1 ,1 , 2, 4, 3]
    >>> liste_initiale.count(1)
    3

    >>> liste_initiale.count(4)
    1

    >>> liste_initiale.count(23)
    0
    ```



#### G. Supprimer un élément avec son indice `del`

L'instruction `del` supprime un élément d'une liste à un indice déterminé. On notera que bien, qu'il s'agisse d'une méthodes sa syntaxe est différente.

???+ example "Exemple"
    ```python
    >>> liste_initiale = [1, 2, 3]
    >>> del liste_initiale[1]
    >>> liste_initiale
    [1, 3]
    ```


**Remarque**

Contrairement aux méthodes associées aux listes présentées dans cette rubrique, `del` est une instruction générale de Python, utilisable pour d'autres objets que des listes. Celle-ci ne prend pas de parenthèse.




### 3.9 Des méthodes pour les chaînes de caractères

#### A. .split()
La méthodes `.split()` permet de découper un texte et renvoie une liste. Le choix du séparateur se fait en paramètres.

???+ example "Exemple"
    ```python
    >>> nom_couleurs_texte = "rouge, vert, bleu, noir, jaune, violet"
    >>> liste_couleur = nom_couleurs_texte.split(",")
    >>> liste_couleur
    ['rouge', ' vert', ' bleu', ' noir', ' jaune', ' violet']

    >>> texte = "un texte avec quelques mots"
    >>> liste_mots = texte.split(" ")
    >>> liste_mots
    ['un', 'texte', 'avec', 'quelques', 'mots']

    ```


#### B. . upper() et .capitalize()
La méthode `.upper()` transforme tous les caractères en majuscule.
La méthode `.capitalize()` transforme la première lettre en majuscule.


???+ example "Exemple"
    ```python
    >>>texte = "un texte en minuscule"
    >>>texte.upper()
    UN TEXTE EN MINUSCULE

    >>>texte.capitalize()
    Un texte en minuscule
    ```





## 4. Les structures conditionnelles
Python nous fournit plusieurs structures conditionnelles, que nous allons étudier ici :

- La condition (“si”) : `if …`
- La condition (“si…sinon”) : `if … else …`
- La condition (“si…sinon si… sinon”) : `if … elif … else …`
  

### 4.1 Les structures conditionnelles "si" : if …

L’instruction `if` qui veut dire (si) introduit **une expression booléenne**, suivie d’une instruction ou d’un groupe d’instructions qui indique quelle action doit être effectuée si l’expression de test est évaluée à `True`.

En Python, on écrit une structure conditionnelle grâce au mot clé `if`, suivit de la condition à vérifier et deux points `:`. En dessous, on trouve le code à exécuter dans le cas ou la condition est validé. Pour que python soit en mesure de délimiter le code a exécuter, celui-ci est indenté :

```python
if condition:
    code


suite du programme

```

<br/>

Suivant la valeur de la condition, Python exécute ou non le code qui se trouve dans le bloc conditionnel.

- si la condition est vraie (`True`), le code qui se trouve dans le bloc conditionnel est exécuté.
- si la condition est fausse (`False`), le code n'est pas exécuté.

<br/>

???+ example "Exemple"
    ```python
    condition_vrai = True
    condition_fausse = False

    if condition_vrai :
        print("La condition est vrai, le code correspondant à la condition vrai est exécuté !")


    if condition_fausse :
        print("La condition est fausse, ce texte n'est donc pas affiché !")

    print("Le programme est terminé !")
    ```

    <br/>

    ```python
    # Dans la console
    La condition est vrai, le code correspondant à la condition vrai est exécuté !
    Le programme est terminé !
    ```
    <br/>

    On constate que lorsque la condition vaut `True`, le bloc de code est exécuté et le texte : `"La condition est vrai, le code correspondant à la condition vrai est exécuté !" est affiché`.
    Par contre, lorsque la condition vaut `False`, le bloc de code n'est pas exécuté et le programme continue ; la preuve en est que le texte `"Le programme est terminé !"` est affiché. 



!!! note "Remarque"
    En Python,`None` et `0` sont des valeurs interprétées comme `False`.

    ```python
    condition_fausse1 = None
    if condition_fausse :
        print("La condition 1 est fausse, ce texte n'est pas affiché !")

    condition_fausse2 = 0
    if condition_fausse :
        print("La condition 2 est fausse, ce texte n'est pas affiché !")

    print("Le programme est terminé !")
    ```

    <br/>

    ```python
    # Dans la console
    Le programme est terminé !
    ```


### 4.2 La structures "si … sinon …": if … else … 

Il est parfois nécessaire d'exécuter un code, lorsque la condition est vrai et un autre, lorsque la condition est fausse, on utilise alors la structure condition : `if … else …`


```python
if condition :
    code
else :
    code

suite du programme

```


Le bloc de code (indenté) qui suit le `else :` est exécuté pour tout les cas où la condition n'est pas vrai (`True`). C'est à dire pour tout les cas où la condition est évalué à faut (`False`).

<br/>


???+ example "Exemple : La condition est vrai"
    ```python
    condition = True
 
    if condition :
        print("La condition est vrai, le code correspondant à la condition vrai est exécuté !")
    else :
        print("La condition est fausse, le code correspondant à la condition fausse est exécuté !")

    print("Fin du programme !")
    ```
    <br/>

    ```python
    # Dans la console
    La condition est vrai, le code correspondant à la condition vrai est exécuté !
    Fin du programme !
    ```


???+ example "Exemple : La condition est fausse"
    ```python
    condition = False
 
    if condition :
        print("La condition est vrai, le code correspondant à la condition vrai est exécuté !")
    else :
        print("La condition est fausse, le code correspondant à la condition fausse est exécuté !")

    print("Fin du programme !")

    ```

    <br/>

    ```python
    # Dans la console
    La condition est fausse, le code correspondant à la condition fausse est exécuté !
    Fin du programme !
    ```





### 4.3 La structures "si … sinon si … sinon": if … elif … else 

Il est parfois nécessaire de vérifier plusieurs conditions différentes est d'appliquer un code approprié pour chacune de ces conditions. On utilise alors la structure conditionnelles : `if … elif … else` 


```python
if condition1 :
    code
elif condition2 :
    code
else :
    code

suite du code

```

<br/>

On peut écrire autant de `elif` que nécessaire !

<br/>

???+ example "Exemple : Conditions multiples"
    ```python
    condition1_fausse = False
    condition2_fausse = False
    condition3_vrai  = True

 
    if condition1_fausse :
        print("La condition 1 est fausse, le code correspondant ne sera pas exécuté et donc ce texte ne sera pas affiché !")

    elif condition2_fausse :
        print("La condition 2 est fausse, le code correspondant ne sera pas exécuté et donc ce texte ne sera pas affiché !")

    elif condition3_vrai :
        print("La condition 3 est vrai, le code correspondant à cette condition est exécuté !")

    else :
        print(" Une des conditions est vrai, le code qui suit l'instruction else ne sera pas exécuté et donc ce texte ne sera pas affiché !")

    print("Fin du programme !")

    ```

    <br/>

    ```python
    # Dans la console
    La condition 3 est vrai, le code correspondant à cette condition est exécuté !
    Fin du programme !
    ```

<br/>

Cependant, lorsqu'une condition est vérifiée et que sont bloc de code est exécuté, les conditions qui suivent ne seront pas vérifiée !

??? example "Exemple : Conditions multiples"
    ```python
    condition1_vrai = True
    condition2_fausse = False
    condition3_vrai  = True

 
    if condition1_vrai :
        print("La condition 1 est vrai, le code correspondant à cette condition est exécuté !")

    elif condition2_fausse :
        print("La condition 2 est fausse, le code correspondant ne sera pas exécuté et donc ce texte ne sera pas affiché !")

    elif condition3_vrai :
        print("Bien que la condition 3 soit vrai, le code correspondant à cette condition n'est pas exécuté !
        Un bloc précédent dans la structure conditionnelle à déjà été exécuté, la condition 3 n'a même pas été évalué !")

    else :
        print(" Une des conditions est vrai, le code qui suit l'instruction else ne sera pas exécuté et donc ce texte ne sera pas affiché !")

    print("Fin du programme !")

    ```

    <br/>

    ```python
    # Dans la console
    La condition 3 est vrai, le code correspondant à la condition vrai est exécuté !
    Fin du programme !
    ```


### 4.4 Les conditions et les comparaisons
#### A. Les conditions

Nous avons vu, que les structures conditionnels fonctionnent avec des conditions. Généralement, ces conditions font appel à des comparaisons.

???+ example "Exemple"
    ```python
    age = 15

    if age < 18 :
        print(age < 18)
        print("Vous êtes mineur !")

    print("Fin du programme")

    ```

    ```python
    # Dans la console
    True
    Vous êtes mineur !
    Fin du programme
    ```

    Dans ce cas, la condition est une comparaison entre la valeur de la variable `age` et l'entier `18`. `age` est bien inférieur à `18`, la condition (`age < 18`) vaut `True`.



#### B. Les comparaisons

Python utilise toute une série de comparateur :

<center>

Syntaxe Python | Signification
---------   |   ----------
`==` | égal à
`!=` | différent de
`>`  | supérieur à
`>=` | supérieur ou égal à
`<` | inférieur à
`<=` | inférieur ou égal à

</center>

Python renvoie la valeur `True`, si la comparaison est vraie et `False` si elle est fausse. `True` et `False` sont des booléens (un nouveau type de variable).

???+ example "Exemple"
    ```python
    >>> x > 10
    False

    >>> x < 10
    True

    >>> x = 5
    >>> y = 5
    >>> x == y
    True

    >>> x = 5
    >>> x == 5
    True

    ```

<br/>

!!! warning "Attention"
    Faites bien attention à ne pas confondre l'opérateur d'affectation `=` qui affecte une valeur à une variable et l'opérateur de comparaison `==` qui compare les valeurs de deux variables.

<br/>

Vous pouvez également effectuer des comparaisons sur des chaînes de caractères.
???+ example "Exemple"
    ```python
    >>> animal = "tigre"
    >>> animal == "tig"
    False

    >>> animal != "tig"
    True

    >>> animal == "tigre"
    True
    ```

<br/>

Dans le cas des chaînes de caractères, a priori seuls les tests `==` et `!=` ont un sens. En fait, on peut aussi utiliser les opérateurs `<`, `>`, `<=` et `>=`. Dans ce cas, l'ordre alphabétique est pris en compte.

???+ example "Exemple"
    ```python
    >>> "a" < "b"
    True
    ```

    <br/>

    "a" est inférieur à "b" car le caractère a est situé avant le caractère b dans l'ordre alphabétique. En fait, c'est l'ordre ASCII des caractères qui est pris en compte (à chaque caractère correspond un code numérique), on peut donc aussi comparer des caractères spéciaux (comme # ou ~) entre eux.
    
    <br/>

    On peut également comparer des chaînes de caractères de plusieurs caractères :

    ```python
    >>> "ali" < "alo"
    True

    >>> "abb" < "ada"
    True
    ```

    <br/>

    Dans ce cas, Python compare les deux chaînes de caractères, caractère par caractère, de la gauche vers la droite (le premier caractère avec le premier, le deuxième avec le deuxième, etc). Dès qu'un caractère est différent entre l'une et l'autre des deux chaînes, il considère que la chaîne la plus petite est celle qui présente le caractère ayant le plus petit code ASCII (les caractères suivants de la chaîne de caractères sont ignorés dans la comparaison), comme dans l'exemple "abb" < "ada" ci-dessus.



## 5. Les boucles

??? note "vidéo"
    <center><iframe width="560" height="315" src="https://www.youtube.com/embed/Oidy_JUbOOY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><center/>
    

### 5.1 Boucles bornées `for`

??? note "vidéo"
    <center><iframe width="560" height="315" src="https://www.youtube.com/embed/9B0eWgbI528" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><center/>

En programmation, on est souvent amené à répéter plusieurs fois une instruction. Incontournables à tout langage de programmation, les boucles vont nous aider à réaliser cette tâche de manière compacte et efficace.

<br/>

Imaginez par exemple que vous souhaitiez afficher les éléments d'une liste les uns après les autres. Dans l'état actuel de vos connaissances, il faudrait taper quelque chose du style :

???+ example "Exemple"
    ```python
    animaux = ["girafe", "tigre", "singe", "souris"]
    print(animaux[0])
    print(animaux[1])
    print(animaux[2])
    print(animaux[3])
    ```

Si votre liste contient uniquement quatre éléments ceci est encore faisable, mais imaginez qu'elle en contienne cent où mille !

<br/>

Pour remédier à cela, il faut utiliser une boucle `for`.

```python
for element in liste :
    code

suite du code

```

Dans ce cas `element` prend la valeur de chaque élément de `liste` et exécute le bloc de code indenté en dessous.
Ainsi le bloc de code indenté est exécuté autant de foi qu'il y a d'élément dans `liste`.

<br/>

???+ example "Exemple"
    ```python
    >>> animaux = ["girafe", "tigre", "singe", "souris"]
    >>> for animal in animaux :
    ...     print(animal)
    girafe
    tigre
    singe
    souris
    ```



### 5.2 Boucles non bornées `while`

??? note "vidéo"
    <center><iframe width="560" height="315" src="https://www.youtube.com/embed/5l7gIfDJxkg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><center/>

Une autre alternative à l'instruction `for` couramment utilisée en informatique est la boucle `while` "tant que".

<br/>

Le principe est simple. Une série d'instructions est exécutée tant qu'une condition est vraie.

???+ example "Exemple"
    ```python
    >>> i = 1
    >>> while i <= 4:
    ...     print(i)
    ...     i = i + 1
    ...
    1
    2
    3
    4
    ```

Remarquez qu'il est encore une fois nécessaire d'indenter le bloc d'instructions correspondant au corps de la boucle (ici, les instructions lignes 3 et 4).

<br/>

Une boucle `while` nécessite généralement trois éléments pour fonctionner correctement :

- Initialisation de la variable d'itération avant la boucle (ligne 1).
- Test de la variable d'itération associée à l'instruction `while` (ligne 2).
- Mise à jour de la variable d'itération dans le corps de la boucle (ligne 4).

<br/>

Faites bien attention aux tests et à l'incrémentation que vous utilisez car une erreur mène souvent à des « boucles infinies » qui ne s'arrêtent jamais. Vous pouvez néanmoins toujours stopper l'exécution d'un script Python à l'aide de la combinaison de touches Ctrl-C (c'est-à-dire en pressant simultanément les touches Ctrl et C).

???+ example "Exemple"
    ```python
    i = 0
    while i < 10:
        print("Le python c'est cool !")
    ```

    <br/>

    Ici, nous avons omis de mettre à jour la variable `i` dans le corps de la boucle. Par conséquent, la boucle ne s'arrêtera jamais (sauf en pressant Ctrl-C) puisque la condition `i < 10` sera toujours vraie.

<br/>


La boucle `while` combinée à la fonction `input()` peut s'avérer commode lorsqu'on souhaite demander à l'utilisateur une valeur numérique.

???+ example "Exemple"
    ```python
    i = 0

    while i < 10 :
        print(f"Début du bloc, {i} est bien inférieur à 10 !")
        reponse = input("Entrez un entier : ")
        i = int(reponse)
        print(f"L'utilisateur à saisie {i}, cette ligne est affiché quelques soit la valeur saisie par l'utilisateur !")

    print("Fin du programme")
    ```

    <br/>

    ```python
    "Début du bloc, 0 est bien inférieur à 10 !"

    # ouverture d'une boite de dialogue 
    Entrez un entier : 9

    "L'utilisateur à saisie 9, cette ligne est affiché quelques soit la valeur saisie par l'utilisateur !"



    "Début du bloc, 9 est bien inférieur à 10 !"

    # ouverture d'une boite de dialogue 
    Entrez un entier : 15

    "L'utilisateur à saisie 15, cette ligne est affiché quelques soit la valeur saisie par l'utilisateur !"

    "Fin du programme"
    ```

    <br/>

    La fonction `input()` prend en argument un message (sous la forme d'une chaîne de caractères), demande à l'utilisateur d'entrer une valeur et renvoie celle-ci sous forme d'une chaîne de caractères. Il faut ensuite convertir cette dernière en entier (avec la fonction `int()`).



## 6. Les fonctions (partie 2)
### 6.1 Écrire ces propres fonctions

Pour définir une fonction, Python utilise le mot-clé `def`, suivi du nom que l'on souhaite donner à la fonction, puis de parenthèses
et on finit la ligne par `:` .


Le code que l'on souhaite voir exécuté lors de l'appel à la fonction est écrit en dessous avec une indentation.


```python
def nom_fonction():
    code

```

<br/>

Lorsque la fonction ne renvoie rien, on dit qu'il s'agit d'une procédure.

???+ example "Exemple"
    ```python
    def bienvenu():
        print("Affichage d'un message de bienvenu !")

    bienvenu()
    ```
    ```python
    "Affichage d'un message de bienvenu !"
    ```

<br/>

Pour qu'une fonction soit qualifiée de fonction, elle doit renvoyer quelque chose, il faut alors utiliser le mot-clé `return`.

```python
def nom_fonction():
    code
    return("ce que l'on veut renvoyer")

```


???+ example "Exemple"
    ```python
    def une_fonction():
        return("Résultat renvoyé par la fonction !")

    valeur = une_fonction()
    print("La fonction à été exécutée, car cette ligne est affiché et pourtant rien n'est apparu !")
    print(valeur)
    ```
    ```python
    "La fonction à été exécutée, car cette ligne est affiché et pourtant rien n'est apparu !"
    "Résultat renvoyé par la fonction !"
    ```



<br/>

**Passage d'arguments**

Lors de la définition d'une fonction, il est possible d'utiliser un ou plusieurs arguments. Le nombre d'argument est laissé libre à l'initiative du programmeur qui développe une nouvelle fonction.



???+ example "Exemple"
    ```python
    def volume_cube(arrete):
        resultat = arrete * arrete * arrete
        return (resultat)

    arrete_du_cube = 15
    volume = volume_cube(arrete_du_cube)
    print(f"Le volume du cube d'arrête {arrete_du_cube}cm est de {volume}cm3.")

    ```
    ```python
    Le volume du cube d’arrête 15cm est de 3375 cm3.
    ```

    Ici, le résultat renvoyé par la fonction "volume_cube()" est stocké dans la variable "volume". 



???+ example "Exemple"
    ```python
    def multiplier(x, y):
        return x*y
    ```
    ```python
    multiplier(2, 3)
    6
    ```

## 7. Les modules, librairies, bibliothèques
### 7.1 Définition

Un package (une bibliothèque en français) est une collection de fonctions qui peuvent être ajoutées au code Python et appelées au besoin, comme n’importe quelle autre fonction. Il n’y a aucune raison de réécrire du code pour réaliser une tâche standard. Avec les packages, vous pouvez importer des fonctions préexistantes et étendre efficacement les fonctionnalités de votre code.

<br/>

Pour être utilisées, les packages doivent être préalablement installé !

!!! note "Remarque"
    Les packages utilisé dans le cadres des cours au lycées seront déjà installé !  La partie concernant leur installation ne sera donc pas développé ici, cependant si vous utiliser un ordinateur personnel, cela sera peut être nécessaire. Dans ce cas, reportez vous au cours : [https://openclassrooms.com/fr/courses/7168871-apprenez-les-bases-du-langage-python/7296681-importez-des-packages-python](https://openclassrooms.com/fr/courses/7168871-apprenez-les-bases-du-langage-python/7296681-importez-des-packages-python)


<br/>

Une fois le package installé sur votre poste de travail, il est nécessaire de l'importé dans votre script python pour pouvoir l'utilisé.

On utilise alors le mot-clé  `import` tout en haut de votre fichier de code, vous pourrez importer certaines fonctions d’un package ou le package entier d’un coup.

???+ example "Exemple"
    ```python
    import math

    print(math.log(2.7183))
    print(math.log(2))

    ```
    ```python
    1.0000066849139877
    0.6931471805599453
    0.0

    ```

### 7.2 Math
Ce module est toujours disponible, car il est installé lors de l'installation de Python.

Il fournit l’accès à de nombreuses fonctions mathématiques.


- `math.sqrt(x)`, Renvoie la racine carrée de x.
- `math.exp(x)`, Renvoie e**x.
- `math.log(x)`, Renvoie le logarithme naturel de x.
- `math.log2(x)`, Renvoie le logarithme en base 2 de x.
- `math.cos(x)`, Renvoie le cosinus de x radians.
- `math.sin(x)`, Renvoie le sinus de x radians.
- `math.tan(x)`, Renvoie la tangente de x radians.
- `math.pi`, renvoie la valeur de la constante mathématique pi.
- …



### 7.3 Tracer des courbes : Matplotlib
Pour tracer des courbes, Python n’est pas suffisant et nous avons besoin de la bibliothèque `Matplotlib` . 
Pour en savoir plus sur le fonctionnement de matplotlib :

- [https://courspython.com/introduction-courbes.html](https://courspython.com/introduction-courbes.html)

