---
title : "Activité 1"
correctionvisible : true

---

# Activité 1 : Que savez-vous vous d’internet ?


- Pour vous, qu’est-ce qu’internet ?
- Dessiner internet

??? note "pdf"
    <center>
    <iframe src="../pdf/activite1.pdf" height="800" width="1200"></iframe>
    </center>

!!! faq correction "Correction"
    À faire

<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  