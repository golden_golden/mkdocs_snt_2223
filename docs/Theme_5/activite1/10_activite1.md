---
title : "Activité 1"
correctionvisible : True

---


<div class="lien_pdf_sujet">
  <a  href="../pdf/2_SNT_Theme5_Activite1.pdf">Sujet.pdf </a>
  

  <a href="../pdf/correction/2_SNT_Theme5_Activite1_correction.pdf" class="correction">Correction.pdf</a>
</div>



<center>
![Sujet activité 2](pdf/2_SNT_Theme5_Activite1.webp){width="1200" loading="lazy"}
</center>






<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  