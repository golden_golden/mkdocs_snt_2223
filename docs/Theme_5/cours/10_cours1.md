---
title : "Activité 1"
correctionvisible : true

---

# Activité 1

??? faq correction "Correction"
    <center>
        <iframe src="../pdf/correction/2_SNT_Theme5_Activite1_correction.pdf" height="800" width="1200"></iframe>
    </center>


<center>
    <iframe src="../pdf/2_SNT_Theme5_Activite1.pdf" height="800" width="1200"></iframe>
</center>





<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  