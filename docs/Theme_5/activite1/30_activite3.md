---
title : "Activité 3"
correctionvisible : "False"

---


<div class="lien_pdf_sujet">
  <a  href="../pdf/2_SNT_Theme5_Activite3.pdf">Sujet.pdf </a>
  

  <a href="../pdf/correction/2_SNT_Theme5_Activite3_correction" class="correction">Correction.pdf</a>
</div>





# Activité 3 :  Histoire de la cartographie et des GNSS



### **Document 1 :** Histoire de la cartographie

- [https://ladigitale.dev/digiview/#/v/65079e886d5c7](https://ladigitale.dev/digiview/#/v/65079e886d5c7)



### **Document 2 :** Histoire des systèmes de positionnement par satellites (GNSS)

- [https://ladigitale.dev/digiview/#/v/65079cf941f49](https://ladigitale.dev/digiview/#/v/65079cf941f49)
- [https://ladigitale.dev/digiview/#/v/65079d67e54e9](https://ladigitale.dev/digiview/#/v/65079d67e54e9)


<br>

### **Questions :**

1.Quelle technique l’abbé Picard a t'il utilisé pour établir ces cartes ? Comment procède-t-on avant ?<br>
2.Quel inconvénient majeur possède la carte de Cassini ?<br>
3.Comment est représenté le relief sur la carte d’état-major ?<br>
4.Comment est représenté le relief sur les cartes de randonnées actuelles ?<br>
5.Qu’est-ce que la photogrammétrie aérienne ?<br>
6.Comment s’appelle le premier satellite de l’histoire ?<br>
7.Comment s’appeler le premier système de positionnement ? Fonctionnait il grâce à des satellites ?<br>
8.Pour quelle raison le système NAVSTAR est il accessible aux activités civiles ?<br>
9.Quelle est la précision du GPS ?<br>
10.Pourquoi l'Europe s’est-elle dotée de son propre système de positionnement par satellites ? Quels en sont les avantages ?<br>
11.Quelle est la précision du système Galileo ?<br>
12.En quelle année sort la première version de Google Earth ?<br>
13.Depuis quelle année peut-on voir les photos panoramiques street view ?<br>








<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  