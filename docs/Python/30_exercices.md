---
title : "Exercices"

---

# Exercices


### Exercice 1

- Indiquez le type des variables permettant de stocker (sur votre smartphone) les informations suivantes :

    - *le nom d’un contact*
    - *le numéro de téléphone d’un contact*
    - *un SMS*
    - *l’heure du réveil*
    - *le code de votre partage de connexion Wi-Fi*
    - *le pourcentage affiché de batterie restante*
    - *les notes aux deux derniers devoirs de Physiques que vous tapez dans l’application Calculatrice pour calculer votre moyenne*




### Exercice 2

- Indiquez la valeur des variables à l’issue de chaque ligne du programme suivant.

```python
a = 15
b = 10
c = a + b
d = c / 2
```

- Quel est le type de chaque variable ?


- On considère maintenant que `a` et `b` correspondent à des notes. Réécrivez (sur papier) le programme en utilisant des noms de variables plus représentatifs (pour les 4 variables).


### Exercice 3
On considère le programme Python suivant.
```python
a = 8
b = 3
a = a - 4
b = 2 * b
a = a + b
print(a)
```


- Combien de variables sont utilisées ? Combien de valeurs différentes ont-elles prises au cours de l’exécution du programme ?

- Quelle est la valeur finale de la variable `a` ?

- Cliquez ici pour ouvrir l'éditeur de code. Vérifiez votre réponse à la question précédente en exécutant le code : pour cela il suffit de cliquer sur le bouton Exécuter.

- Il est possible d’afficher plusieurs valeurs avec la fonction `print()`. Par exemple, si on veut afficher les valeurs des variables a et b on écrit simplement `print(a, b)`. Modifiez la dernière ligne du programme et exécutez-le.

### Exercice
On considère le programme Python suivant.

```python
a = 5
b = a + 1
b = b + 2
c = b - a
print(c)
```

- Qu’affiche ce programme ?
- Recopiez à la main ce programme dans l'ide ci-dessous(sans copier-coller, c'est pour s'habituer à la syntaxe de python) puis vérifiez votre réponse précédente en exécutant ce programme dans l’éditeur.

{{ IDE() }}


### Exercice
- On considère le programme de calcul suivant.

    - *A prend la valeur 5*
    - *Multiplier A par 3*
    - *Soustraire 4 au résultat*
    - *Élever le résultat au carré*
    - *Afficher le résultat*

- Écrivez dans l'ide Python ci-dessous ce programme de calcul. Vérifiez ensuite en l'exécutant.


{{ IDE() }}


??? note "Liens"
    [https://hub-binder.mybinder.ovh/user/pythonlycee-pylyc-fbai4dfy/notebooks/Syntaxes_elementaires_Python.ipynb](https://hub-binder.mybinder.ovh/user/pythonlycee-pylyc-fbai4dfy/notebooks/Syntaxes_elementaires_Python.ipynb)
    [https://www.python-lycee.com/parcours-apprentissage](https://www.python-lycee.com/parcours-apprentissage)
    [https://www.python-lycee.com/parcours-apprentissage-pl1](https://www.python-lycee.com/parcours-apprentissage-pl1)
    [https://www.lyceum.fr/2gt/snt/0-programmation-python](https://www.lyceum.fr/2gt/snt/0-programmation-python)
    [https://python.sdv.univ-paris-diderot.fr/](https://python.sdv.univ-paris-diderot.fr/)

