---
title : "CSS"

---

# Chapitre 2 : HTML et CSS 


## 3. Un peu de CSS
### 3.1 Le code CSS.

!!! note "le fichier css"
    Télécharger le fichier CSS : "style.css"
    Le placer dans le dossier nommé : "site_base"
    Ouvrir Édupython, puis dans Édupython ouvrir le ficher "style.css".


### 3.2 Premières notions de CSS

### 3.3 Le modèle des boites
#### A. Tout est boite
L’idée centrale du modèle des boites est que tout élément HTML peut être représenté par un empilement de différentes boites rectangulaires :

- La première boite, centrale, va être composée du contenu de l’élément en soi ;
- La deuxième boite va être composée de la première boite ainsi que des marges internes de l’élément ;
- La troisième boîte va être composée de la deuxième boite et des bordures de l’élément ;
- La quatrième boite va être composée de la troisième boite et des marges externes de l’élément.

<br>

Voici la représentation d’un élément selon le modèle des boites :


<figure markdown>
  ![Illustration et explication du modèle des boites CSS](images/modele-boites-css.png){ width="500" }
  <figcaption>Illustration et explication du modèle des boites CSS</figcaption>
</figure>


<br>


- [developer.mozilla__margin](https://developer.mozilla.org/fr/docs/Web/CSS/margin)
- [developer.mozilla__padding](https://developer.mozilla.org/fr/docs/Web/CSS/padding)





 
#### B. Les propriétés CSS liées aux différentes boites

Le CSS va déjà nous fournir différentes propriétés qui vont nous permettre de spécifier la taille des différents éléments composants les différentes boites :

- Les propriétés width et height vont nous permettre de définir la largeur et la hauteur de la boite « contenu » ;
- La propriété padding va nous permettre de définir la taille des marges internes ;
- La propriété border va nous permettre de définir des bordures pour notre élément ;
- La propriété margin va nous permettre de définir la taille des marges externes.

<br>

Nous allons dans cette partie commencer avec l’étude de ces différentes propriétés qui sont fondamentales. Nous parlerons également de la propriété box-sizing qui va nous permettre de changer la façon dont la largeur et la hauteur d’un élément vont être calculées et donc de modifier le modèle des boites par défaut.

 
#### C. Le modèle des boites et le positionnement

Comprendre comment est calculée la taille de chaque élément et de quoi chaque élément est composé est essentiel pour créer des mises en page efficaces.

<br>

Pour choisir le type d’affichage et de positionnement des éléments HTML, le CSS va nous fournir des propriétés très puissantes qui vont nous permettre de modifier le flux normal de la page, c’est-à-dire de modifier l’ordre d’affichage des éléments ou la place réservée par défaut à chacun d’entre eux.

<br>

Ici, nous allons nous intéresser aux propriétés suivantes :

- La propriété display qui va nous permettre de définir un type d’affichage pour un élément ;
- La propriété position qui va nous permettre de positionner nos éléments de différentes façons dans une page ;
- La propriété float qui va nous permettre de faire « flotter » des éléments HTML dans la page.








### 3.4 Positionner les éléments




[developer.mozilla___CSS_Flow_Layout](https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Flow_Layout)
[developer.mozilla__display](https://developer.mozilla.org/fr/docs/Web/CSS/display)
[developer.mozilla__position](https://developer.mozilla.org/fr/docs/Web/CSS/position)
[https://www.youtube.com/watch?v=uo13gKY6q5k](https://www.youtube.com/watch?v=uo13gKY6q5k)
[https://www.youtube.com/watch?v=jx5jmI0UlXU](https://www.youtube.com/watch?v=jx5jmI0UlXU)