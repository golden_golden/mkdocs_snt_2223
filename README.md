# SNT

Cours de SNT
- **Niveau** : Seconde
- **Année scolaire** : 2022-2023
- **Établissement** : Lycée Bossuet *(Condom)*


## Où voir le site ?
[C'est ici](https://golden_golden.gitlab.io/mkdocs_snt_2223/)



## Techno
Site réalisé avec :  *mkdocs*

Merci, aux enseignents du [forum de NSI](https://mooc-forums.inria.fr/) pour la découverte de cet outil, et notament à :
- Vincent BOUILLO pour ses dépots [gitlab](https://gitlab.com/bouillotvincent) et [ici](https://gitlab.com/ferney-nsi)
- Franck CHAMBON (Francky) pour ses dépots [gitlab](https://gitlab.com/ens-fr)



## Project status
En cours de développement.
 


## Authors
**Initial**  : VILLEMUR Arnaud

**Contributeurs** : …

*Ajouter votre nom si vous avez contribué !*



## License
Copyright © 2022 VILLEMUR Arnaud <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a>.
