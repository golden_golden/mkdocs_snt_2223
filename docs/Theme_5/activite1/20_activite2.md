---
title : "Activité 2"
correctionvisible : True

---

<div class="lien_pdf_sujet">
  <a  href="../pdf/2_SNT_Theme5_Activite2.pdf">Sujet.pdf </a>
  

  <a href="../pdf/correction/2_SNT_Theme5_Activite2_correction.pdf" class="correction">Correction.pdf</a>
</div>


<center>
![Sujet activité 2](pdf/2_SNT_Theme5_Activite2_p1.webp){width="1200"  loading="lazy"}
![Sujet activité 2](pdf/2_SNT_Theme5_Activite2_p2.webp){width="1200"  loading="lazy"}
</center>

<br>

[Lien vidéo](https://ladigitale.dev/digiview/#/v/64fec2e6266a2)

<br>

??? note "Vidéo"

    **Quelques liens sur le fonctionnement des GNSS**:
    
    - [Géolocalisation par satellite - Principes de fonctionnement Galileo](https://www.youtube.com/watch?v=HLnijHcrKxI)
    - [LES HORLOGES DES SATELLITES DE NAVIGATION PEUVENT-ELLES MESURER LA TERRE ?](https://www.youtube.com/watch?v=gw7CrBjdgS4)
    - [LE GPS](https://www.youtube.com/watch?v=PdIVGVvWH0Y)
    - [LE GPS ET LA RELATIVITÉ](https://www.youtube.com/watch?v=1GEwDVyjTtw)




<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  