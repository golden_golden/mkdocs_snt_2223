---
title : "Activité 7"
correctionvisible : False

---

# Activité 7


<div class="lien_pdf_sujet">
  <a  href="../pdf/2_SNT_Chapitre5_Activité4.pdf">Sujet.pdf </a>
  

  <a href="../pdf/correction/2_SNT_Chapitre5_Activité4.pdf" class="correction">Correction.pdf</a>
</div>




<center>
    <iframe src="../pdf/2_SNT_Chapitre5_Activité4.pdf" height="800" width="1200"></iframe>
</center>





<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  