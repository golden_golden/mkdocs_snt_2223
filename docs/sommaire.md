---
title : "Sommaire"

---

# Sommaire


## Thème 1 : Localisation, cartographie et mobilité
- Principe de fonctionnement des GNSS
- Se repérer sur terre
- Protocole NMEA 0183
- Cartes numériques
- Calculs d'itinéraires
- Confidentialité 


