---
title : "Ressources"

---

# Chapitre 2 : HTML et CSS 



## 9. Ressources

Cours complet :


<br>

Cours en vidéo :

- [https://www.youtube.com/watch?v=8FqZZrbnwkM](https://www.youtube.com/watch?v=8FqZZrbnwkM)
- [vidéo 1](https://www.youtube.com/watch?v=u5W2NWItytc)
- [vidéo 2](https://www.youtube.com/watch?v=oEAuNzWXRjM)
- [vidéo 3](https://www.youtube.com/watch?v=qsbkZ7gIKnc&list=PLEiMYEzpB4QtYf4F6PwW57f971VqUzGhv)
- [vidéo 4](https://www.youtube.com/watch?v=iSWjmVcfQGg&list=PLEiMYEzpB4QtYf4F6PwW57f971VqUzGhv&index=2)
- [vidéo 5](https://www.youtube.com/watch?v=rRt-yvGQST0)
- [vidéo 6](https://www.youtube.com/watch?v=6hCGTJCo_Uo)

<br>

Mémento language :

- [https://jaetheme.com/balises-html5/#a](https://jaetheme.com/balises-html5/#a)
- [https://www.w3schools.com/tags/default.asp](https://www.w3schools.com/tags/default.asp)
- [https://www.w3schools.com/cssref/index.php](https://www.w3schools.com/cssref/index.php)
- [https://simon.html5.org/html-elements](https://simon.html5.org/html-elements)
- [http://www.robot24.fr/wp-content/3eme/siteweb/index.html#19](http://www.robot24.fr/wp-content/3eme/siteweb/index.html#19)
- [http://www.robot24.fr/wp-content/3eme/siteweb/chapitre4/memento-des-proprietes-css.html](http://www.robot24.fr/wp-content/3eme/siteweb/chapitre4/memento-des-proprietes-css.html)
- [https://ducom.me/hema/creation-site/Topics/02a-body.html](https://ducom.me/hema/creation-site/Topics/02a-body.html)

- [https://ducom.me/hema/creation-site/Topics/04d-principes.html](https://ducom.me/hema/creation-site/Topics/04d-principes.html)
- [https://ducom.me/hema/creation-site/Tasks/05b-creation.html](https://ducom.me/hema/creation-site/Tasks/05b-creation.html)
- [https://aymeric-auberton.fr/academie/html/memento/](https://aymeric-auberton.fr/academie/html/memento/)


<br>

Ressources :

- [https://ducom.me/hema/creation-site/Topics/03-presentation-css.html](https://ducom.me/hema/creation-site/Topics/03-presentation-css.html)