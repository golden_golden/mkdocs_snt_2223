---
title : "HTML"

---

# Chapitre 2 : HTML et CSS 


## 1. Comment fonctionne un site web ?

Un site web n’est qu’un ensemble de fichiers et de ressources (pages de code de différents types et fichiers médias comme des images, etc.) liés entre eux.

<br>

Toutes ces ressources vont être hébergées (stockées) sur un ordinateur très puissant et constamment connecté à Internet qu’on appelle serveur, dans un dossier principal qu’on va appeler la racine d’un site.

<br>

Pour pouvoir stocker nos différentes ressources sur un serveur et faire en sorte que notre site soit tout le temps accessible via Internet, nous passons généralement par un hébergeur qui va nous louer un serveur ou une partie d’un de ses serveurs.

<br>

Dans ce dossier racine, il va très souvent y avoir d’autres dossiers (des sous-dossiers donc), des fichiers, des images, etc.





## 2. HTML et CSS, deux langages incontournables du web


Les langages HTML et CSS vont se trouver à la base de tout projet web. Les navigateurs (Google Chrome, Safari, etc.) sont des programmes qui sont construits pour pouvoir lire que du code HTML, CSS et JavaScript et nous allons donc nous appuyer sur ces langages pour pouvoir afficher nos pages.



Quel que soit votre projet web *(blog, site e-commerce, application mobile, etc.)*, vous devrez forcément utiliser du HTML et du CSS.

<br>


Le HTML est un langage de structure : il permet d’indiquer au navigateur que tel contenu est un titre, que tel autre est un simple texte, que cet objet est une image, que celui-ci est une liste, etc. Le navigateur, qui « comprend » le HTML, va se baser sur ces indications pour afficher les contenus.

<br>

Le CSS permet d'avoir la main sur le style, autrement dit le look de votre site web. C'est lui qui rendra votre site web moderne et créatif.


Dans un premier temps, étudions le code HTML.

## 3. Code HTML
### 3.1 À quoi ressemble le code HTML 
!!! note "le fichier html"
    Télécharger le fichier HTML *(`clic droit` sur le lien, puis `enregistrer la cible du lien sous`)* : [`page.html`](documents/page.html)

    <br>

    Le placer dans un nouveau dossier nommé : `site_base`
    
    <br>

    Ouvrir Édupython, puis dans Édupython, ouvrir le ficher : `page.html`.

    *(`fichier`, `ouvrir`, si votre fichier n’apparaît pas c'est peut être du à l'affichage, par défaut Édupython n'affiche que les fichiers `.py`, il faut changer la sélection de l'affichage en bas à droite de la fenêtre et choisir `Tous les fichiers`.)*



#### B. Les éléments HTML
Le langage HTML tout entier repose sur l’utilisation d’éléments.

<br>

Dans une page, nous allons utiliser les éléments en HTML pour marquer du contenu, c’est-à-dire pour lui donner du sens aux yeux des navigateurs et des moteurs de recherche. On va utiliser des éléments pour définir un paragraphe *(avec l'élément `p`)* ou un titre *(avec l'élément `h`)* par exemple, ou encore pour insérer une image *(avec l'élément `img`)* ou un lien *(avec l'élément `a`)* dans un document.





#### B. Les balises HTML

Un élément HTML peut être soit constitué d’une paire de balises (ouvrante et fermante) et d’un contenu, soit d’une balise unique qu’on dit alors « orpheline ».

<br>
**Les balises en paires**

L’élément `p` *(qui sert à définir un paragraphe)* est par exemple constitué d’une balise ouvrante, d’une balise fermante et d’un contenu textuel entre les balises. Le texte contenu entre les deux balises va être le texte considéré par le navigateur comme étant un paragraphe. Voici comment on va écrire cela :


<figure markdown>
  ![Composition de l'élément `p`](images/16693910453649_FR_1603881_HTML-CSS_Static-Graphics_p1c3-1.jpg){ width="500" }
  <figcaption>Composition de l'élément `p`</figcaption>
</figure>


Les balises de l’élément sont constituée d’un chevron ouvrant`<`, du nom de l’élément en question et d’un chevron fermant `>`.

<br>

Notez bien, la différence entre la balise ouvrante et la balise fermante de notre élément `p` : la balise fermante contient un slash `/` avant le nom de l’élément.





<br><br>




**Les balises orphelines**

Certains éléments en HTML ne vont être constitués que d’une balise qu’on appelle alors orpheline. Cela va être le cas pour certains éléments qui ne possèdent pas de contenu textuel comme l’élément `br` par exemple qui sert simplement à créer un retour à la ligne en HTML et qui va s’écrire comme ceci :


<figure markdown>
  ![Composition de l'élément `br`](images/element-br-exemple-html.png){ width="500" }
  <figcaption>Composition de l'élément `br`</figcaption>
</figure>


#### C. La structure minimale d’une page HTML

Pour qu’une page HTML soit déclarée valide, elle doit obligatoirement comporter certains éléments et suivre un schéma précis.

Voici ci-dessous le code minimum pour créer une page HTML valide.



<figure markdown>
  ![structure minimale d'un page html](images/16693917384741_FR_1603881_HTML-CSS_Static-Graphics_p1c3-3.jpg){ width="600" }
  <figcaption>Structure minimale d'un page `html`</figcaption>
</figure>


??? note "Remarque"
    Vos pages HTML devraient toujours être valides, car une page non valide ne sera pas comprise par le navigateur qui va alors potentiellement mal l’afficher voire dans certains cas ne pas l’afficher du tout.

    <br>

    De plus, une page non valide sera également mal analysée par les moteurs de recherche et ces mêmes moteurs de recherche risquent donc de ne pas la valoriser, c’est-à-dire de ne pas la proposer aux utilisateurs recherchant une information que votre page contient. En d’autres termes, posséder des pages non valides risque d’impacter négativement le référencement de ces pages et de votre site en général dans les moteurs de recherche.




#### D. L'indentation
La majorité du code html se trouvant entre des paires de balises, qui de plus peuvent être imbriquées, il est nécessaire pour s'y retrouver d'utiliser l'indentation.

<br>

Indenter correspond à créer des retraits en début de ligne dans votre éditeur de code de façon cohérente et logique.

<br>

**Un code HTML non indenté :**

<figure markdown>
  ![Code HTML non indenté](images/code-html-non-indente.png){ width="800" }
  <figcaption>Code HTML non indenté</figcaption>
</figure>




**Un code HTML correctement indenté :**

<figure markdown>
  ![Code HTML correctement indenté](images/code-html-indente.png){ width="800" }
  <figcaption>Code HTML correctement indenté</figcaption>
</figure>


Indenter va nous permettre de discerner plus facilement les différents éléments ou parties de code, le code est plus propre et plus lisible, donc plus compréhensible. La recherche des erreurs est également facilitée.



### 3.2 Lien entre le code et le site web

Pour voir ce que donne notre code, c'est facile !
Il suffit d'ouvrir notre document `page.html` avec un navigateur Web *(Chrome,Mozilla…)*.





### 3.3 Mes premiers éléments HTML

#### A. Les paragraphes
Pour créer des paragraphes en `HTML`, nous allons utiliser l’élément `p`.

<br>

On peut créer autant de paragraphes que l’on souhaite dans une page. À chaque nouveau paragraphe, il faut utiliser un nouvel élément `p`.

<br>

Pour chaque nouveau paragraphe, un retour à la ligne va être créé automatiquement et affiché par votre navigateur.




<figure markdown>
  ![Image title](https://dummyimage.com/600x400/eee/aaa){ width="600" }
  <figcaption>Élements paragraphe `p`</figcaption>
</figure>





Notons que si dans le corps d'un paragraphe, on saute plein de lignes, ou que l'on utilise un nombre d'espaces important, cela n’apparaît pas dans l’aperçu final !


??? note "Remarque"
    On peut donc utiliser les espaces et les saut de lignes comme bon nous semble pour rendre le code plus lisible, sans se soucier de l’aperçu final.



Pour effectuer un retour à la ligne visible dans le rendu final, on utilise l’élément `br` qui est représenté par une unique balise orpheline`<br>`.Le nom de l’élément `br` est l’abréviation de « break », l’équivalent de « casser » en anglais (dans le sens de « casser une ligne »).

<br>

On peut utiliser autant d’éléments `br` que l’on souhaite au sein d’un titre ou d’un paragraphe par exemple.

<br>

Pour sauter une ligne, on en utilise deux `<br><br>` (mais dans ce ca il est généralement possible ou préférable de faire deux paragraphes distinct.)














#### B. Les titres
Il existe six niveaux hiérarchiques de titres *(noté « h » pour « heading » en anglais, équivalent au mot « titre » en français)* définis par les éléments `h1`, `h2`, `h3`, `h4`, `h5` et `h6` qui vont nous permettre d’organiser le contenu dans nos pages.


<figure markdown>
  ![Les titres en html](images/definition-titres-h-html.png){ width="600" }
  <figcaption>Les titres en html</figcaption>
</figure>

<figure markdown>
  ![Les titres en html](images/affichage-titres-h-html.png){ width="600" }
  <figcaption>Les titres en html</figcaption>
</figure>

L’élément `h1` représente un titre principal dans notre page ou dans une section de page et, à ce titre, nous n’allons pouvoir utiliser qu’un seul élément `h1` par page.

En revanche, nous allons pouvoir utiliser autant de titres de niveau `h2`, `h3` etc. que l’on souhaite dans notre page. Théoriquement, si nos pages sont bien construites, nous ne devrions que rarement dépasser le niveau de titre `h3`.





### 3.4 Les liens et Les images en HTML


#### A. Les attributs HTML
Les éléments vont également pouvoir contenir des attributs qu’on va alors placer au sein de la balise ouvrante. Pour certains éléments, les attributs vont être facultatifs tandis que pour d’autres ils vont être obligatoires pour garantir le bon fonctionnement du code `HTML`.

<br>

Les attributs vont venir compléter les éléments en les définissant plus précisément ou en apportant des informations supplémentaires sur le comportement d’un élément. Un attribut contient généralement une valeur.

<br>


#### B. Les attributs des images

L’élément `img`, sert à insérer une image dans une page HTML, va lui nécessiter deux attributs qui sont les attributs `src` et `alt`.


<br>

L’attribut `src` (pour « source ») va prendre comme valeur l’adresse de l’image tandis que l’attribut `alt` (pour « alternative ») va nous permettre de renseigner une description textuelle de l’image qui sera affichée dans les cas où l’image ne serait pas disponible pour une raison ou une autre : image introuvable, impossible à charger, etc.

<br>

L’attribut `alt` va également se révéler indispensable pour rendre notre site accessible aux non-voyants ou aux mal voyants et pour leur fournir une bonne expérience de navigation puisqu’ils sont généralement équipés de lecteur spéciaux qui vont pouvoir lire la valeur de l’attribut `alt` et donc leur permettre de se faire une représentation du contenu de l’image.

<figure markdown>
  ![Attributs des images](images/element-img-attribut-src-alt-html.png){ width="500" }
  <figcaption>Composition d'un élément HTML `img` avec `src` et `alt`</figcaption>
</figure>

??? note "Remarque"
    L’élément `img` n’est constitué que d’une seule balise orpheline, tout comme l’élément `br` vu précédemment. On place dans ce cas les attributs au sein de la balise orpheline.


#### C. Les attributs des liens

Prenons ici l’exemple de l’élément `a` qui est l’abréviation de « anchor » ou « ancre » en français. Cet élément va principalement nous servir à créer des liens vers d’autres pages, ou partie de page.

<br>

Pour le faire fonctionner correctement, nous allons devoir lui ajouter un attribut `href` pour « hypertexte reference » ou « référence hypertexte » en français.

<br>

En effet, c’est l’attribut `href` qui va nous permettre de préciser la cible du lien, c’est-à-dire la page de destination du lien en lui passant l’adresse de la page en question en valeur.




<figure markdown>
  ![Attributs des liens](images/element-a-attribut-href-html.png){ width="500" }
  <figcaption>Composition d'un élément HTML `a` avec `href`</figcaption>
</figure>








#### C. Les liens
Les liens hypertextes sont l’une des fondations du HTML qui est un langage de marquage hypertexte justement.

<br>

Les liens en HTML vont nous servir à créer des ponts entre différentes pages d’un même site ou de sites différents. Le principe d’un lien est le suivant : en cliquant sur une ancre (qui est la partie visible par l’utilisateur d’un lien et qui peut être un texte comme une image), nos utilisateurs vont être redirigés vers une page cible.

<br>

Il existe deux types principaux de liens hypertextes en HTML :

- Les liens internes qui vont servir à naviguer d’une page à l’autre dans un même site, ou bien à l’intérieur même d'une page ;
- Les liens externes qui vont envoyer les utilisateurs vers une page d’un autre site.

<br>

Quel que soit le type de liens que l’on souhaite créer en HTML, nous utiliserons toujours l’élément `a` qui est l’abréviation de « anchor » ou « ancre » en français accompagné de son attribut `href` pour « hypertext reference » ou « référence hypertexte » en français.


<br>


L’élément HTML `a` est composé d’une paire de balises (balises ouvrante et fermante) et d’un contenu entre les balises que l’on va appeler “ancre”. Ce contenu peut être un texte, une image, etc. et sera la partie visible et cliquable du lien pour les utilisateurs.

<br>

L’attribut `href` va nous servir à indiquer la cible du lien, c’est-à-dire l’endroit où l’utilisateur doit être envoyé après avoir cliqué sur le lien. Nous allons indiquer cette cible en valeur de l’attribut href.


<br>
D’un point de vue du code, la seule chose qui va changer pour créer un lien ancre plutôt qu’interne ou interne plutôt qu’externe va être la façon dont on va construire la valeur qu’on va passer à l’attribut `href`.

<br>

Les liens en HTML pour naviguer au sein d’une même page et renvoyer à un endroit précis de celle-ci. sont appeller des liens « ancres » tout simplement (même si ce terme peut porter à confusion car le terme « ancre » est aussi utilisé pour définir la partie visible et cliquable d’un lien).


 
**Créer des liens externes en HTML**

<br>

Un lien externe est un lien qui part d’une page d’un site et ramène les utilisateurs vers une autre page d’un autre site.

<br>

Pour créer un lien vers une page d’un autre site en HTML (ou lien externe), il va falloir indiquer l’adresse complète de la page (c’est-à-dire son URL) en valeur de l’attribut href de notre lien.

<br>

Bon à savoir : Lorsque l’attribut href prend une URL complète en valeur, on parle de valeur absolue (car celle-ci est fixe et ne dépend de rien). Les liens externes utilisent toujours des valeurs absolues. On parle de valeur absolue en opposition aux valeurs dites relatives, qui sont dans ce contexte des valeurs qui vont indiquer l’emplacement de la page cible relativement à la page source du lien.


<br>

**Créer des liens internes en HTML**

<br>

Le deuxième grand type de liens que l’on va pouvoir créer en HTML correspond aux liens internes, c’est-à-dire à des liens renvoyant vers d’autres pages d’un même site.

<br>

Nous allons avoir plusieurs solutions pour créer des liens internes. Tout d’abord, nous allons tout simplement pouvoir faire exactement comme pour les liens externes et indiquer l’adresse complète (adresse absolue) de la page cible du lien en valeur de l’attribut `href`

<br>

Cependant, si cette première manière de faire semble tout à fait fonctionner et être la plus simple à priori, ce n’est pas du tout la plus optimisée d’un point de vue de l’utilisation des ressources et elle peut même s’avérer problématique selon comment votre site est construit (notamment si vos URLs sont construites dynamiquement).

<br>

Pour ces raisons, nous préciserons généralement plutôt une valeur de type relatif en valeur de l’attribut `href` de nos liens internes en HTML.

<br>

On dit que la valeur est relative car on va devoir indiquer l’adresse de la page de destination relativement à l’adresse de la page de départ (c’est-à-dire celle à partir de laquelle on fait notre lien).

<br>

Comment savoir quelle valeur relative utiliser ? 

<br>

Structure site dossier

<br>

Pour lier ces différents fichiers et ressources entre eux (et donc offrir entre autres à nos utilisateurs la possibilité de naviguer de l’un à l’autre), nous allons utiliser des valeurs relatives, c’est-à-dire que nous allons préciser l’emplacement de la ressource que l’on souhaite utiliser (ou vers laquelle on souhaite renvoyer) relativement à l’emplacement de la page / ressource qui souhaite l’utiliser (la page de départ).

<br>

C’est comme cela que nous allons procéder dans le cas précis de la création de liens internes : nous allons préciser en valeur de l’attribut `href` l’emplacement de la page cible (page de destination) par rapport à l’emplacement sur le serveur de la page source.

<br>

Trois cas vont alors se présenter à nous :

- Le cas où les deux pages (source et destination) se trouvent dans un même dossier ;
- Le cas où la page de destination se trouve dans un sous dossier par rapport à la page source ;
- Le cas où la page de destination se trouve dans un dossier parent par rapport à la page source.

<br>

Pour chacun de ces cas, nous allons construire la valeur de notre attribut href de manière différente :

- Si les deux pages se situent dans le même dossier, alors on pourra se contenter de préciser le nom de la page cible/de destination (avec son extension) en valeur de l’attribut href ;
- Si la page cible se situe dans un sous dossier par rapport à la page à partir de laquelle on fait un lien, alors on précisera le nom du sous dossier suivi d’un slash suivi du nom de la page cible en valeur de l’attribut href ;
- Si la page cible se situe dans un dossier parent par rapport à la page de départ, alors il faudra indiquer deux points suivi d’un slash suivi du nom de la page de destination en valeur de l’attribut href.


<br>



### 3.4 Les autres éléments HTML 


mémento : HTML 









