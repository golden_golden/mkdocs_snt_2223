---
title : "Les bases"
correctionvisible : true

---

# Les bases

!!! faq correction "Correction"
    <center>
    <iframe src="../pdf/les_bases_correction.pdf" height="800" width="1200"></iframe>
    </center>

<center>
<iframe src="../pdf/les_bases.pdf" height="800" width="1200"></iframe>
</center>


<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  