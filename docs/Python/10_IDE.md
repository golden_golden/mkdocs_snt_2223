---
title : "Où faire du Python ?"

---

# Où écrire et exécuter un script Python ?

## 1. Coder sans installations
### 1.1 Solution 1 : Ici
La page que vous êtes en train de lire !


#### A. IDE

{{ IDE() }}

#### B. Console

{{ terminal() }}


### 1.2 Solution 2 : Basthon
Basthon : [https://console.basthon.fr/](https://console.basthon.fr/)

## 2. Coder au lycée
Pour coder au lycée, vous avez deux possibilités :

- Soit utiliser une des solutions en lignes cité précédemment.
- Soit utiliser Édupython qui est installé sur toutes les machines du lycée.
(logicieldisponibles/math/Édupython)


## 3. Coder sur son ordinateur personnel 
Je vous recommande deux solutions :

- Édupython (déjà présent sur les ordinateurs région):
- Thonny :